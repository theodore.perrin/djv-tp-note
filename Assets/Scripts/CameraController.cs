using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform playerTarget;
    [SerializeField] float camSens = 2f; //Sensibilité de la caméra
    [SerializeField] private float dist = 0.25f; //Distance au joueur
    private Vector2 rotation = Vector2.zero;
    private float sideOffset = 1f;
    [SerializeField] private float verticalLimit = 60f;
    public float collisionOffset = 0.1f; //Empeche de traverser les murs
    public float cameraSpeed = 15f;
    public LayerMask cameraOcclusion;
    Vector3 desiredPos;
    private Vector3 velocity;



    // Start is called before the first frame update
    void Start()
    {
        playerTarget = transform.parent;
        desiredPos = transform.localPosition;

        //Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        rotation.y += Input.GetAxis("Mouse X") * camSens;
        rotation.x += -Input.GetAxis("Mouse Y") * camSens;
        rotation.x = Mathf.Clamp(rotation.x, -verticalLimit, verticalLimit);
        transform.rotation = Quaternion.Euler(rotation);
        desiredPos = 1f * Vector3.up + sideOffset * transform.right - dist * transform.forward;
        RaycastHit hit;
        Vector3 dirTmp = desiredPos.normalized;
        if (Physics.Raycast(playerTarget.position, dirTmp, out hit, Vector3.Magnitude(desiredPos), cameraOcclusion))
        {
            desiredPos = 1f * Vector3.up + sideOffset * transform.right + dirTmp * (hit.distance - collisionOffset);
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, desiredPos, ref velocity, 0.3f);
        }
        else
        {
            transform.localPosition = desiredPos;
        }
    }
}
