using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] private float _speed = 20f;
    private int _damage;
    [SerializeField] private float acceleration;
    [SerializeField] private LayerMask bulletCollidable;
    [SerializeField] private GameObject explosion;
    private Vector3 _lastPos;
    private RaycastHit _hit;
    public GameObject cantHit;


    public void Init(int damage)
    {
        _damage = damage;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _lastPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(_speed * Time.deltaTime * Vector3.forward);
        if (Physics.SphereCast(_lastPos, 0.2f,  transform.position - _lastPos, out _hit,
                Vector3.Magnitude(_lastPos - transform.position), bulletCollidable))
        {
            GameObject enemy = _hit.collider.gameObject;
            if (enemy.TryGetComponent<Character>(out var enemyCharacter) && (cantHit is null || enemy != cantHit))
            {
                enemyCharacter.ApplyDamage(_damage);
            }
            Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        _lastPos = transform.position;
        _speed += acceleration * Time.deltaTime;
    }
    
    
}

