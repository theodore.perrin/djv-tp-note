using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Timeline;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class EnemyDistance : EnemyCharacter
{


    protected float FireRate = 3f;
    protected void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    protected void OnEnable()
    {
        StartCoroutine(Coroutine());
        IEnumerator Coroutine()
        {
            yield return null;
            _navMeshAgent.enabled = true;

            while (enabled)
            {
                do
                {
                    _navMeshAgent.SetDestination(target.position + 15f * (transform.position - target.position).normalized);
                    transform.rotation = Quaternion.LookRotation(new Vector3(target.position.x - transform.position.x, 0, target.position.z - transform.position.z));
                    yield return null;
                }
                while (_navMeshAgent.hasPath);

                // Destination reached, wait before moving again.

                Attack();
                yield return new WaitForSeconds(FireRate);
            }
        }
    }

    protected abstract void Attack();

    
    
}