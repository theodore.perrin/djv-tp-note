using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class SwordEnemy : EnemyMelee
{
    private Character _char;
    public LayerMask characterMask;
    [SerializeField] private int baseDamage = 2;
    [SerializeField] private int damageScale = 1;
    [SerializeField] private int damageScaleFrequency = 2;
    
    [SerializeField] private float baseSpeed = 3.5f;
    [SerializeField] private float speedScale = 1f;
    [SerializeField] private float maxSpeed = 10f;
    [SerializeField] private int speedScaleFrequency = 2;
    
    private int Damage => baseDamage + damageScale * (Game.Instance.WaveNumber / damageScaleFrequency);
    private Vector3 _lastPos;
    private float Speed => Vector3.SqrMagnitude((transform.position - _lastPos) / Time.deltaTime);
    [SerializeField] private Animator _animator;
    private readonly int _speed = Animator.StringToHash("Speed");
    private readonly int _attack = Animator.StringToHash("Attack");

    protected override void Start()
    {
        base.Start();
        _lastPos = transform.position;
        float speedRatio = Mathf.Exp(-speedScale * (speedScaleFrequency / Game.Instance.WaveNumber));
        _navMeshAgent.speed = speedRatio * baseSpeed + (1 - speedRatio) * maxSpeed;
    }
    private void Update()
    {
        _animator.SetFloat(_speed, Speed);
        _lastPos = transform.position;
    }

    protected override void Attack()
    {
        _animator.SetTrigger(_attack);
        StartCoroutine(AttackCoroutine());

        IEnumerator AttackCoroutine()
        {
            yield return new WaitForSeconds(0.5f);
            foreach (Collider col in Physics.OverlapBox(transform.position + 0.5f * Vector3.up + 2f * transform.forward,
                         new Vector3(0.5f, 0.01f, 0.75f),
                         transform.rotation,
                         characterMask))
            {
                _char = col.gameObject.GetComponent<Character>();
                _char.ApplyDamage(Damage);
            }
        }
    }
    
}
