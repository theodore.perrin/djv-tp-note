using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardEnemy : EnemyDistance
{
    private Character _char;
    [SerializeField] private Transform FireballSpawnPos;
    [SerializeField] private GameObject Fireball;
    [SerializeField] private int baseDamage = 1;
    [SerializeField] private int damageScale = 4;
    [SerializeField] private int damageScaleFrequency = 3;
    
    [SerializeField] private float baseFireRate = 3f;
    [SerializeField] private float fireRateScale = 2f;
    [SerializeField] private float minFireRate = 1f;
    [SerializeField] private float fireRateScaleFrequency = 5;
    
    [SerializeField] private float baseSpeed = 3.5f;
    [SerializeField] private float speedScale = 0.5f;
    [SerializeField] private float maxSpeed = 5f;
    [SerializeField] private int speedScaleFrequency = 2;
    
    private int Damage => baseDamage + damageScale * (Game.Instance.WaveNumber / damageScaleFrequency);
    private Vector3 _lastPos;
    
    private float Speed => Vector3.Dot(transform.forward, (transform.position - _lastPos) / Time.deltaTime); //Indique la direction du mouvement (avant/arrière)
    [SerializeField] private Animator _animator;
    private readonly int _speed = Animator.StringToHash("Speed");
    private readonly int _attack = Animator.StringToHash("Attack");

    protected override void Start()
    {
        base.Start();
        _lastPos = transform.position;
        float speedRatio = Mathf.Exp(-speedScale * ( speedScaleFrequency / Game.Instance.WaveNumber));
        float fireRateRatio = Mathf.Exp(-fireRateScale * (fireRateScaleFrequency / Game.Instance.WaveNumber));
        FireRate = fireRateRatio * baseFireRate + (1 - fireRateRatio) * minFireRate;
        _navMeshAgent.speed = speedRatio * baseSpeed + (1 - speedRatio) * maxSpeed;
    }
    private void Update()
    {
        _animator.SetFloat(_speed, Speed);
        _lastPos = transform.position;
    }

    protected override void Attack()
    {
        _animator.SetTrigger(_attack);
        StartCoroutine(AttackCoroutine());

        IEnumerator AttackCoroutine()
        {
            yield return new WaitForSeconds(0.5f);
            GameObject newFireball = Instantiate(Fireball, FireballSpawnPos.position,
                Quaternion.LookRotation(target.position + 1.5f * Vector3.up - FireballSpawnPos.position));
            newFireball.GetComponent<Fireball>().Init(Damage);
            newFireball.SetActive(true);
        }
    }
}
