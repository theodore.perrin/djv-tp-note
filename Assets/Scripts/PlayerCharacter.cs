using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(CharacterController))]
public class PlayerCharacter : Character
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletSpawnPos;
    
    [SerializeField] private float fireDelay = 0.5f;
    [SerializeField] private float jumpHeight = 5f;
    
    [SerializeField] private float speed = 4f;
    [SerializeField] private float runSpeed = 6f;
    [SerializeField] private float dashSpeed = 12f;
    [SerializeField] private float dashCD = 5f;
    
    [SerializeField] private int RunUnlockWave = 5;
    [SerializeField] int DashUnlockWave = 10;
    
    
    private new int maxHP => 10 + 2 * (Game.Instance.WaveNumber / 3);
    private readonly float _gravity = -20f;
    private float _yVelocity;
    private float _latestShot;
    private float _latestDash;
    private bool _grounded;
    private bool _isRunning;
    private bool _isAttacking;
    private int previousMaxHP = 10;
    private bool CanRun => Game.Instance.WaveNumber >= RunUnlockWave;
    private bool CanDash => Game.Instance.WaveNumber >= DashUnlockWave;
    private bool _isDashing;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private LayerMask bulletCollisionMask;
    private RaycastHit _hit;

    private Animator _animator;
    private readonly int _speed = Animator.StringToHash("Speed");
    private readonly int _jump = Animator.StringToHash("Jump");
    private readonly int _attack = Animator.StringToHash("Attack");
    private readonly int _death = Animator.StringToHash("Death");
    private readonly int _damage = Animator.StringToHash("Damage");
    private readonly int _damageID = Animator.StringToHash("DamageID");
    private Camera _mainCamera;
    private CharacterController _characterController;

    protected void Awake()
    {
        _animator = GetComponent<Animator>();
        _mainCamera = Camera.main;
        _characterController = GetComponent<CharacterController>();
        Game.Instance.UpdatePlayer(gameObject);
        _isRunning = false;
        _isAttacking = false;
        _isDashing = false;
    }

    private bool stopAttack()
    {
        return Input.GetMouseButtonUp(0);
    }

    IEnumerator Dash()
    {
        _isDashing = true;
        float initTime = Time.time;
        float curTime = initTime;
        while(curTime - initTime <= 1f)
        {
            curTime = Time.time;
            Vector3 forward = _mainCamera.transform.forward;
            _characterController.Move(forward  * (dashSpeed * Time.deltaTime));
            yield return null;
        }

        _latestDash = Time.time;
        _isDashing = false;
    }
    
    protected void Update()
    {
        if (_isDead || _isDashing)
        {
            return;
        }
        _grounded = _characterController.isGrounded;
        if (_grounded && _yVelocity < 0)
        {
            _yVelocity = -1f;
        }

        if (CanRun && Input.GetButtonDown("Run"))
        {
            _isRunning = !_isRunning;
        }

        if (CanDash && Input.GetButtonDown("Dash") && Time.time - _latestDash >= dashCD)
        {
            StartCoroutine(Dash());
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            _isAttacking = true;
            StartCoroutine(IsAttackingCoroutine());

            IEnumerator IsAttackingCoroutine()
            {
                yield return new WaitUntil(stopAttack);
                yield return new WaitForSeconds(Mathf.Max(0, fireDelay - Time.time + _latestShot));
                _isAttacking = false;
            }
        }

        _animator.SetBool(_attack, _grounded && _isAttacking);
        if (_grounded && Input.GetMouseButton(0) && Time.time - _latestShot >= fireDelay)
        {
            _isRunning = false;
            Vector2 screenCenter = new Vector2(Screen.width / 2f, Screen.height / 2f);
            Ray ray = _mainCamera.ScreenPointToRay(screenCenter);
            if (Physics.Raycast(ray, out _hit, 1000f, bulletCollisionMask))
            {
                Vector3 dir = _hit.point - bulletSpawnPos.position;
                GameObject newBullet = Instantiate(bullet, bulletSpawnPos.position, Quaternion.LookRotation(dir));
                newBullet.GetComponent<Bullet>().cantHit = gameObject;
                newBullet.SetActive(true);
                _latestShot = Time.time;
            }
            
        }
        
        float moveForwardInput = Input.GetAxis("Vertical");
        float strafeInput = Input.GetAxis("Horizontal");
        Vector3 forward = new Vector3(_mainCamera.transform.forward.x, 0, _mainCamera.transform.forward.z);
        Vector3 right = new Vector3(forward.z, 0, -forward.x);
        if (!_isRunning)
        {
            _characterController.Move(forward * (moveForwardInput * speed * Time.deltaTime));
            _characterController.Move(right * (strafeInput * speed * Time.deltaTime));
            _animator.SetFloat(_speed, Mathf.Max(Mathf.Abs(moveForwardInput), Mathf.Abs(strafeInput)) * speed);
        }
        else
        {
            _characterController.Move(forward * (moveForwardInput * runSpeed * Time.deltaTime));
            _characterController.Move(right * (strafeInput * runSpeed * Time.deltaTime));
            _animator.SetFloat(_speed, Mathf.Max(Mathf.Abs(moveForwardInput), Mathf.Abs(strafeInput)) * runSpeed);
        }
        transform.LookAt(transform.position + forward);
        if (_grounded && Input.GetButtonDown("Jump"))
        {
            if (!_isAttacking)
            {
                _animator.SetTrigger(_jump);
            }
            _yVelocity += Mathf.Sqrt(jumpHeight * -3.0f * _gravity);
        }
        if (!_grounded)
        {
            _yVelocity += _gravity * Time.deltaTime;
        }
        _characterController.Move(new Vector3(0, _yVelocity * Time.deltaTime, 0));
        
    }
    

    public override void ApplyDamage(int value)
    {
        if (_isDashing) //Dash invincible
        {
            return;
        }

        if (value > 0)
        {
            _animator.SetInteger(_damageID, Random.Range(1, 4));
            _animator.SetTrigger(_damage);
        }
        
        HP = Math.Clamp(HP - value, 0, maxHP);
        RefreshHealthBar();
        if (HP <= 0)
        {
            Die();
        }
    }

    public void RefreshHealthBar()
    {
        //Soigne si on a gagné de la vie max
        HP += maxHP - previousMaxHP;
        previousMaxHP = maxHP;
        
        healthBar.SetActive(HP < maxHP);
        HPScript.UpdateBar(HP/ (float) maxHP);
    }
    
    protected override void Die()
    {
        _animator.SetTrigger(_death);
        StartCoroutine(DeathCoroutine());

        IEnumerator DeathCoroutine()
        {
            healthBar.SetActive(false);
            yield return new WaitForSeconds(4f);
            _mainCamera.GetComponent<CameraController>().enabled = false;
            Game.Instance.MainUI.SetActive(false);
            Game.Instance.GameOverUI.SetActive(true);
        }
    }
    
}

