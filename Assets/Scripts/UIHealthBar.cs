using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHealthBar : MonoBehaviour
{
    [SerializeField] private RectTransform barTransform;
    [SerializeField] private RectTransform fillTransform;
    [SerializeField] private Transform _camera;

    // Start is called before the first frame update
    private void Start()
    {
        _camera = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        barTransform.rotation = _camera.rotation;
    }

    public void UpdateBar(float ratio)
    {
        fillTransform.anchorMax = new Vector2(ratio, 1);
    }
}
