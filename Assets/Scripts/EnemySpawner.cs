using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [System.Serializable]
    struct Enemy
    {
        public GameObject enemyObject;
        public int waveRequirement;
        public int weight;
    }
    [SerializeField] private Enemy[] objectsToSpawn;
    
    [SerializeField] private float spawnDelay = 2f;
    private int _enemyCap = 1;
    private int WaveNumber => Game.Instance.WaveNumber;

    private float _lastSpawned;
    

    private bool IsWaveOver()
    {
        return EnemyCharacter.AliveEnemies == 0;
    }
    private void OnEnable()
    {
        StartCoroutine(SpawnWave());

        IEnumerator SpawnWave()
        {
            yield return new WaitForSeconds(0.3f);
            while (enabled)
            {
                Game.Instance.NextWave();
                _enemyCap = WaveNumber;
                
                Enemy[] spawnableEnemies = objectsToSpawn.Where(e => e.waveRequirement <= WaveNumber).ToArray();
                int totalweight = 0;
                foreach (Enemy e in spawnableEnemies)
                {
                    totalweight += e.weight;
                }
                float spawnDelayRatio = Mathf.Exp(-WaveNumber);
                float waveSpawndelay = spawnDelayRatio * spawnDelay + (1 - spawnDelayRatio) * 0.5f * spawnDelay;
                for (int i = 0; i < _enemyCap; i++)
                {
                    yield return new WaitForSeconds(Random.Range(waveSpawndelay, spawnDelay));
                    Vector3 spawnPos = Random.insideUnitSphere * 15f;
                    spawnPos.y = 0;
                    int chosenWeight = Random.Range(0, totalweight);
                    int curWeight = 0;
                    foreach (Enemy e in spawnableEnemies)
                    {
                        curWeight += e.weight;
                        if (curWeight > chosenWeight)
                        {
                            GameObject newEnemy = Instantiate(e.enemyObject, spawnPos, Quaternion.identity);
                            newEnemy.SetActive(true);
                            _lastSpawned = Time.time;
                            break;
                        }
                    }
                }

                yield return new WaitForSeconds(0.1f); 
                yield return new WaitUntil(IsWaveOver);
            }
        }
    }
    
    
}
    
