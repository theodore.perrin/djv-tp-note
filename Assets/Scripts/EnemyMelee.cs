using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Timeline;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class EnemyMelee : EnemyCharacter
{
    

    protected void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    protected void OnEnable()
    {
        StartCoroutine(Coroutine());
        IEnumerator Coroutine()
        {
            yield return null;
            _navMeshAgent.enabled = true;

            while (enabled)
            {
                do {
                    _navMeshAgent.SetDestination(target.position + 2f * (transform.position - target.position).normalized);

                    yield return null;
                }
                while (_navMeshAgent.hasPath);

                // Destination reached, wait before moving again.

                transform.LookAt(target, Vector3.up);
                Attack();
                yield return new WaitForSeconds(3f);
            }
        }
    }

    protected abstract void Attack();

    
    
}
