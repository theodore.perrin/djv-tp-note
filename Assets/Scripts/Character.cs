using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] protected int maxHP = 10;
    public int HP { get; protected set; }
    [SerializeField] private GameObject explosion;
    [SerializeField] protected GameObject healthBar;
    protected UIHealthBar HPScript;
    protected bool _isDead => HP <= 0;

    protected virtual void Start()
    {
        HP = maxHP;
        healthBar.SetActive(false);
        HPScript = healthBar.GetComponent<UIHealthBar>();
    }


    public virtual void ApplyDamage(int value)
    {
        HP = Math.Clamp(HP - value, 0, maxHP);
        healthBar.SetActive(HP < maxHP);
        HPScript.UpdateBar(HP/ (float) maxHP);
        if (HP <= 0)
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
