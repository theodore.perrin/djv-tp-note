using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI EnemyText;
    [SerializeField] private TextMeshProUGUI WaveText;
    public void Start()
    {
        Game.Instance.UpdateMainUI(gameObject);
    }

    public void UpdateWave(int wave)
    {
        WaveText.text = string.Format("Wave {0}", wave);
    }
    public void UpdateEnemiesKilled(int enemiesKilled)
    {
        EnemyText.text = string.Format("Enemies killed : {0}", enemiesKilled);
    }
}
