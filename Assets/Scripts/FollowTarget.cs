using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform target;

    protected void LateUpdate()
    {
        transform.position = target.position;
    }
}
