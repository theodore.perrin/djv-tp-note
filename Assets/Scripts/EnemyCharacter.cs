using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyCharacter : Character
{
    public static int AliveEnemies { get; private set; } = 0;
    protected Transform target;
    protected GameObject UICanvas;
    protected NavMeshAgent _navMeshAgent;
    [SerializeField] private GameObject _healPack;
    public static int EnemiesKilled { get; protected set; } = 0;

    [SerializeField] protected float HealDropRate = 0.1f;
    
    private MainUI textScript;

    protected override void Start()
    {
        UICanvas = Game.Instance.MainUI;
        base.Start();
        target = Game.Instance.Player.transform;
        textScript = UICanvas.GetComponent<MainUI>();
        AliveEnemies += 1;
    }
    protected override void Die()
    {
        float healDrop = Random.Range(0f, 1f);
        if (healDrop <= HealDropRate)
        {
            GameObject healPack = Instantiate(_healPack, transform.position + 0.5f * Vector3.up, Quaternion.identity);
            healPack.SetActive(true);
        }
        base.Die();
    }
    private void OnDestroy()
    {
        AliveEnemies -= 1;
        EnemiesKilled += 1;
        textScript.UpdateEnemiesKilled(EnemiesKilled);
    }

}
