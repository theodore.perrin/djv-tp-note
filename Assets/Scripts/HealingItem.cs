using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingItem : MonoBehaviour
{
    private Animator _animator;
    private int _collectTrigger;

    private void Start()
    {
        _animator = transform.GetChild(0).gameObject.GetComponent<Animator>();
        _collectTrigger = Animator.StringToHash("Collect");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<PlayerCharacter>(out var playerScript) == false)
        {
            return;
        }
        Debug.Log("Heal collected");
        playerScript.ApplyDamage(-10);
        _animator.SetTrigger(_collectTrigger);
        Destroy(gameObject, 0.5f);
    }
}
