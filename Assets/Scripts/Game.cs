using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SingletonOptions("GAME", isPrefab: true)]
public class Game : Singleton<Game>
{
    private int _waveNumber;
    public int WaveNumber => _waveNumber;
    
    private GameObject player;

    public GameObject Player => player;
    private PlayerCharacter _playerScript; 
    
    private GameObject _mainUI;

    public GameObject MainUI => _mainUI;
    private MainUI _UIScript;
    
    [SerializeField] private GameObject _gameOverUI;

    public GameObject GameOverUI => _gameOverUI;

    public void Start()
    {
        _waveNumber = 0;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    public void UpdatePlayer(GameObject newPlayer)
    {
        player = newPlayer;
        _playerScript = player.GetComponent<PlayerCharacter>();
    }
    
    public void UpdateMainUI(GameObject newUI)
    {
        _mainUI = newUI;
        _UIScript = _mainUI.GetComponent<MainUI>();
    }

    public void NextWave()
    {
        _waveNumber += 1;
        _UIScript.UpdateWave(_waveNumber);
        _playerScript.RefreshHealthBar(); //Rafraîchit la barre de vie
    }
}
