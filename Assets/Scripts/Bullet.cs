using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed = 40f;
    [SerializeField] private int bulletDamage = 5;
    [SerializeField] private LayerMask bulletCollidable;
    [SerializeField] private GameObject explosion;
    private Vector3 _lastPos;
    private RaycastHit _hit;
    public GameObject cantHit;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _lastPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime * Vector3.forward);
        if (Physics.SphereCast(_lastPos, 0.2f,  transform.position - _lastPos, out _hit,
                Vector3.Magnitude(_lastPos - transform.position), bulletCollidable))
        {
            GameObject enemy = _hit.collider.gameObject;
            if (enemy.TryGetComponent<Character>(out var enemyCharacter) && (cantHit is null || enemy != cantHit))
            {
                enemyCharacter.ApplyDamage(bulletDamage);
            }
            Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        _lastPos = transform.position;
    }
    
    
}
