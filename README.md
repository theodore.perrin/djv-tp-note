# DURENDAL 5552

Une cyborg de 5552 doit revenir dans le passé pour affronter les armées orcs et récupérer Durendal...

## Code source
Le code est disponible sur le dépôt suivant : https://gitlab.pedago.ensiie.fr/theodore.perrin/djv-tp-note

## Objectif
Vaincre le plus d'ennemis possibles.

Les ennemis seront de plus en plus puissants (plus rapides, frapperont plus vite, plus de dégâts), mais le joueur gagne lui aussi en puissance (plus de points de vie, options de mouvement supplémentaires)

## Contrôles
ZQSD pour se déplacer

Clic gauche pour tirer

Espace pour sauter (Attention : on ne peut pas tirer en l'air)

Maj Gauche pour activer/déactiver la course (disponible à partir de la vague 5)

Ctrl Gauche pour un dash invincible (disponible à partir de la vague 10)

## Assets utilisés

Orc : https://assetstore.unity.com/packages/3d/characters/humanoids/fantasy/toon-rts-units-orcs-demo-101359

Magicien : https://assetstore.unity.com/packages/3d/characters/humanoids/fantasy/battle-wizard-poly-art-128097

Cyborg (animator controller modifié) : https://assetstore.unity.com/packages/3d/characters/humanoids/sci-fi/sci-fi-cyborg-girl-35921

Ponts et escalier en bois : https://assetstore.unity.com/packages/3d/props/exterior/modular-wooden-bridge-tiles-29501

Autres props (Material du feuillage modifié avec un Shader customisé pour afficher les 2 faces) : https://assetstore.unity.com/packages/3d/environments/fantasy/stylized-environnement-free-pack-178090

Textures supplémentaires : https://assetstore.unity.com/packages/2d/textures-materials/free-stylized-textures-204742


## Notes

Malheureusement, les animations du modèle du joueur ne sont pas idéales. Notamment, l'animation de tir prend tout le corps et pas simplement les bras, ce qui fait que le personnage "glisse" si il bouge en tirant.